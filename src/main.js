import Vue from 'vue';
import Reveal from 'reveal.js/js/reveal';
import 'reveal.js/css/reveal.css';
import 'reveal.js/css/theme/black.css';
import App from './App.vue';

Vue.config.productionTip = false;

new Vue({
  render(h) {
    return h(App);
  },
  mounted() {
    Reveal.initialize();
  },
}).$mount('#app');
